import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {PermissionsService} from '../../services/permissions.service';
import {ActivatedRoute} from '@angular/router';

interface PermissionsType {
  type: string;
}

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.css']
})
export class PermissionsComponent implements OnInit {
  displayedColumns: string[] = ['command', 'aliases', 'permission'];
  permissionsType: PermissionsType;
  dataSource: MatTableDataSource<[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private route: ActivatedRoute,
              private permissionsService: PermissionsService) {
  }

  ngOnInit() {
    this.route.data.subscribe(l => this.permissionsType = l as PermissionsType);
    if (this.permissionsType.type === 'commands') {
      this.permissionsService.getCommands().subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
      });
    } else {
      this.permissionsService.getFaction().subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
      });
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
