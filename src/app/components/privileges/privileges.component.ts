import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {PermissionsService} from '../../services/permissions.service';

@Component({
  selector: 'app-permissions',
  templateUrl: './privileges.component.html',
  styleUrls: ['./privileges.component.css']
})
export class PrivilegesComponent implements OnInit {
  displayedColumns: string[] = ['privilege', 'permission'];
  dataSource: MatTableDataSource<[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private permissionsService: PermissionsService) { }

  ngOnInit() {
    this.permissionsService.getPrivileges().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
