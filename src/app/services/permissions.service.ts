import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class PermissionsService {

  constructor(private httpClient: HttpClient) { }

  getCommands(): Observable<any> {
    return this.httpClient.get('./assets/commands.json');
  }

  getFaction(): Observable<any> {
    return this.httpClient.get('./assets/faction.json');
  }

  getPrivileges(): Observable<any> {
    return this.httpClient.get('./assets/privileges.json');
  }
}
