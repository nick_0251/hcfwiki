import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {PermissionsComponent} from './components/permissions/permissions.component';
import {MatInputModule, MatListModule, MatPaginatorModule, MatSelectModule, MatSidenavModule, MatTableModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PermissionsService} from './services/permissions.service';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import {PrivilegesComponent} from './components/privileges/privileges.component';

@NgModule({
  declarations: [
    AppComponent,
    PermissionsComponent,
    PrivilegesComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    MatSidenavModule,
    MatListModule,
  ],
  providers: [
    PermissionsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
