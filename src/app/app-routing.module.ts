import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PermissionsComponent} from './components/permissions/permissions.component';
import {HomeComponent} from './components/home/home.component';
import {PrivilegesComponent} from './components/privileges/privileges.component';

const routes: Routes = [
  {
    path: 'permissions/commands',
    component: PermissionsComponent,
    data: { type: 'commands' }
  },
  {
    path: 'permissions/faction',
    component: PermissionsComponent,
    data: { type: 'faction' }
  },
  {
    path: 'permissions/privileges',
    component: PrivilegesComponent,
  },
  { path: '',
    component: HomeComponent
  },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
